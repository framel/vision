#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include "image.h"

float get_pixel(image im, int x, int y, int c)
{
	if (x < im.w && y < im.h && c < im.c)
	{
		return im.data[c * im.h * im.w + y * im.w + x];
	}
	return 0;
}

void set_pixel(image im, int x, int y, int c, float v)
{
	if (x < 0 || y < 0 || c < 0 || x >= im.w || y >= im.h || c >= im.c)
	{
		return;
	}
	im.data[c * im.h * im.w + y * im.w + x] = v;
}

static void crop_copy(image *im, image *crop, int x, int y)
{
	int i;
	int j;
	int k;
	
	i = 0;
	while (i < crop->c)
	{
		j = 0;
		while (j < crop->h)
		{
			k = 0;
			while (k < crop->w)
			{
				crop->data[k + j * crop->w + i * crop->w * crop->h] = im->data[(k + x)  + (j + y) * im->w + i * im->w * im->h];
				++k;
			}
			++j;
		}
		++i;
	}
}

void crop_image_yolo(image *im, image *crop, float x, float y, float w, float h)
{
	crop->w = (int)(im->w * w);
	crop->h = (int)(im->h * h);
	crop->c = im->c;
	crop->data = (float *)malloc(sizeof(float) * crop->w * crop->h * crop->c);
	crop_copy(im, crop, (int)(im->w * x - crop->w / 2), (int)(im->h * y - crop->h / 2));
//printf("x= %f -> %d\n", x, im->w);
//printf("x= %f -> %d\n", im->w * x, (int)(im->w * x));
}


image copy_image(image im)
{
	int end;
    image copy = make_image(im.w, im.h, im.c);

	end = im.w * im.h * im.c;
	for (int i=0; i < end; i++)
	{
		copy.data[i] = im.data[i];
	}
    return copy;
}

image rgb_to_grayscale(image im)
{
    assert(im.c == 3);
    image gray = make_image(im.w, im.h, 1);
    // TODO Fill this in
    return gray;
}

void shift_image(image im, int c, float v)
{
    // TODO Fill this in
}

void clamp_image(image im)
{
    // TODO Fill this in
}


// These might be handy
float three_way_max(float a, float b, float c)
{
    return (a > b) ? ( (a > c) ? a : c) : ( (b > c) ? b : c) ;
}

float three_way_min(float a, float b, float c)
{
    return (a < b) ? ( (a < c) ? a : c) : ( (b < c) ? b : c) ;
}

void rgb_to_hsv(image im)
{
    // TODO Fill this in
}

void hsv_to_rgb(image im)
{
    // TODO Fill this in
}
