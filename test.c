#include "image.h"

int main(void)
{
	image im;
	image cpy;

	im = load_image("image.jpg");
	crop_image_yolo(&im, &cpy, .5, .5, .1, .1);
//	cpy = copy_image(im);
	save_image(cpy, "out");
	free_image(im);
	free_image(cpy);
	return (0);
}
